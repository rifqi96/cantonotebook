<?php

date_default_timezone_set('Asia/Jakarta');
 
$sekarang = new DateTime();
$menit = $sekarang -> getOffset() / 60;
 
$tanda = ($menit < 0 ? -1 : 1);
$menit = abs($menit);
$jam = floor($menit / 60);
$menit -= $jam * 60;
 
$offset = sprintf('%+d:%02d', $tanda * $jam, $menit);

$db_user = "root";
$db_password = "";
$db_name = "cantonotebook";
$db_host = "localhost";

$connect = mysqli_connect($db_host, $db_user, $db_password, $db_name) or die(mysqli_error());
 
mysqli_query($connect, "SET time_zone = '$offset'");

?>
