<html>
<head>
	<title>SHOPPING CART</title>
</head>
<body>
        
	<h2>PURCHASE FORM</h2><br/><br/>
	<?php
	include "images.php";
	?>
	<form method="POST">
		<input type="text" name="nama" placeholder="Name" required/>
		<input type="text" name="alamat" placeholder="Address (Format: Street,City,Province,Zip Code)" required/>
		<input type="text" name="no_hp" placeholder="Phone Number" max="15" required/>
		<input type="text" name="line" placeholder="LINE ID (optional)"/>
		<input type="text" name="qty" placeholder="Quantity" value="<?php echo $_GET['qty'] ?>" min="1" required/>
		<textarea cols="50" rows="4" name="cust_word" placeholder="Custom Your Word! (optional)"></textarea>
		<select name="spiral" size="1">
			<option value="0">Spine</option>
			<option value="1">Spiral (ADD IDR 8,000)</option>
		</select>
		<br/>
		<input type="submit" name="btnBuy" value="BUY"> <button type="reset">RESET</button> <button type="button" onClick="goBack()">BACK</button>
		<script>
		function goBack(){
			window.history.back();
		}
		</script>
	</form>
	
	<?php
	
		if(isset($_POST['btnBuy'])){
			$ip = $_SERVER['REMOTE_ADDR'];
			$nama = $_POST['nama'];
			$alamat = $_POST['alamat'];
			$no_hp = $_POST['no_hp'];
			$line = $_POST['line'];
			$qty = $_POST['qty'];
			$cust_word = $_POST['cust_word'];
			$spiral = $_POST['spiral'];
			$harga_buku = 55000;
			if($qty<1){
				echo "<script> alert('Quantity can't be below than 1'); </script>";
			}
			else{
				if($spiral==0){
					$harga=$qty*$harga_buku;
				}
				else{
					$harga = $qty*($harga_buku+8000);
				}
				$harga_format = number_format($harga);
				$id_buku = $_GET['id_buku'];
				
				$query = mysqli_query($connect,"
				INSERT INTO tb_cust (nama, date, date_expr ,ip, alamat, no_hp, line, qty, cust_word, spiral, harga, id_buku, last_import)
				VALUES ('$nama', now() , DATE_ADD(date,INTERVAL (3*24) DAY_HOUR) ,'$ip' ,'$alamat', '$no_hp', '$line', '$qty', '$cust_word', '$spiral', '$harga', '$id_buku', 1)
				") or die(mysqli_error());
				
				$query2 = mysqli_query($connect,"
				SELECT *
				FROM tb_cust
				WHERE last_import=1
				") or die(mysqli_error());
				
				$row = mysqli_fetch_array($query2);
				
				$id = $row['id_trans'];
				
				echo "<script> alert('THANKS FOR YOUR PURCHASE! Your order has been added successfully. We will inform you where to pay. Your Transaction ID: CT$row[id_trans] And Price = Rp $harga_format');
				window.location.href='index.php'</script>";
				
				$query3 = mysqli_query($connect, "
				UPDATE tb_cust
				SET last_import=0
				") or die(mysqli_error());
			}
		}
	
	?>
	<br>

</body>
</html>