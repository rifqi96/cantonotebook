<?php
if(!isset($_SESSION['username'])){
	if(isset($_GET['module'])){
		if($_GET['module']=="shop" && isset($_GET['route'])){
			include "$_GET[module]/$_GET[route].php";
		}
		else if($_GET['module']!="shop" && $_GET['module']!="admin"){
			include "konten/$_GET[module].php";
		}
		else if($_GET['module']=="admin"){
			include "$_GET[module]/form.php";
		}
	}
	else{
		include "konten/home.php";
	}
}
else{
	if(isset($_GET['module'])){
		if($_GET['module']=="admin" && isset($_GET['route'])){
			include "$_GET[module]/$_GET[route].php";
		}
		else if($_GET['module']=="shop" && isset($_GET['route'])){
			include "$_GET[module]/$_GET[route].php";
		}
		else if($_GET['module']!="shop" && $_GET['module']!="admin"){
			include "konten/$_GET[module].php";
		}
	}
	else{
		include "konten/home.php";
	}
}

?>