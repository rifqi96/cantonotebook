<!-- Bootstrap CSS -->
<link rel="stylesheet" href="assets/bootstrap/bootstrap.min.css">

<div class="container-fluid">
		
<!-- Carousel container -->
<div id="my-pics" class="carousel slide" data-ride="carousel">

<!-- Indicators -->
<ol class="carousel-indicators">
<li data-target="#my-pics" data-slide-to="0" class="active"></li>
<li data-target="#my-pics" data-slide-to="1"></li>
<li data-target="#my-pics" data-slide-to="2"></li>
<li data-target="#my-pics" data-slide-to="3"></li>
<li data-target="#my-pics" data-slide-to="4"></li>
<li data-target="#my-pics" data-slide-to="5"></li>
<li data-target="#my-pics" data-slide-to="6"></li>
<li data-target="#my-pics" data-slide-to="7"></li>
<li data-target="#my-pics" data-slide-to="8"></li>
<li data-target="#my-pics" data-slide-to="9"></li>
<li data-target="#my-pics" data-slide-to="10"></li>
</ol>

<!-- Content -->
<div class="carousel-inner" role="listbox">

<!-- Slide 1 -->
<div class="item active">
<img src="images/portfolio/IMG_2110.JPG" width="50%" height="50">
</div>

<!-- Slide 2 -->
<div class="item">
<img src="images/portfolio/IMG_2111.JPG" width="50%" height="50">
</div>

<!-- Slide 3 -->
<div class="item">
<img src="images/portfolio/IMG_2112.JPG" width="50%" height="50">
</div>

<div class="item">
<img src="images/portfolio/IMG_2113.JPG" width="50%" height="50">
</div>

<div class="item">
<img src="images/portfolio/IMG_2117.JPG" width="50%" height="50">
</div>

<div class="item">
<img src="images/portfolio/IMG_2116.JPG" width="50%" height="50">
</div>

<div class="item">
<img src="images/portfolio/IMG_2118.JPG" width="50%" height="50">
</div>

<div class="item">
<img src="images/portfolio/IMG_2119.JPG" width="50%" height="50">
</div>

<div class="item">
<img src="images/portfolio/IMG_2120.JPG" width="50%" height="50">
</div>

<div class="item">
<img src="images/portfolio/IMG_2121.JPG" width="50%" height="50">
</div>

<div class="item">
<img src="images/portfolio/IMG_2123.JPG" width="50%" height="50">
</div>

</div>

<!-- Previous/Next controls -->
<a class="left carousel-control" href="#my-pics" role="button" dslata-slide="prev">
<span class="icon-prev" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#my-pics" role="button" data-slide="next">
<span class="icon-next" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>

</div>

<!-- Center the image -->
<style scoped>
.item img{
    margin: 0 auto;
}
</style>

</div>
		
<!-- jQuery library -->
<script src="assets/bootstrap/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/bootstrap/bootstrap.min.js"></script>