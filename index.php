<!DOCTYPE HTML>
<?php
session_start();
include "koneksi.php";
?>

<html>
	<head>
		<title>CANTO NOTEBOOK</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="icon" type="img" href="images/icon.jpg"/>
	</head>
	<body>

		<!-- Banner -->
			<section id="banner">
				<a href="index.php"><img src="images/logo.png" style="width:100%" border="0" alt="Null"></a>
				<p>BRING YOUR EXPRESSION ANYWHERE</p>
				<ul class="actions">
					<li><a href="#badan" class="button special">Get started</a></li>
				</ul>
			</section>

		<!-- One -->
			<section id="one" class="wrapper special">
    			<a name="badan" style='text-decoration:none'>
					<?php
						include "validasi.php";
					?>
                </a>
    
			</section>

		<!-- Two -->
<!--			<section id="two" class="wrapper style2 special">
				<div class="inner narrow">
				</div>
			</section>-->

		<!-- Footer -->
			<footer id="footer">
				<div class="copyright">
					&copy;VARIF 2015.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>