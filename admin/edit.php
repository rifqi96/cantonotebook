<html>
<head>
	<title>ADMIN CONTROL PANEL</title>
</head>
<body>
        <?php
		if(isset($_GET['module'])){
			include"menu.php";
		}
		?>
        
	<h2>PURCHASE EDIT</h2>
	
	<form method="POST">
		<input type="text" name="id_buku" placeholder="Book ID" value="<?php echo $_GET['id_buku']; ?>" required/>
		<input type="text" name="nama" placeholder="Name" value="<?php echo $_GET['nama']; ?>" required/>
		<input type="text" name="alamat" placeholder="Address" value="<?php echo $_GET['alamat']; ?>" required/>
		<input type="text" name="no_hp" placeholder="Phone Number" value="<?php echo $_GET['no_hp']; ?>" required/>
		<input type="text" name="line" placeholder="LINE ID (optional)" value="<?php echo $_GET['line']; ?>"/>
		<input type="text" name="qty" placeholder="Quantity" value="<?php echo $_GET['qty'] ?>" value="<?php echo $_GET['qty']; ?>" required/>
		<textarea cols="50" rows="4" name="cust_word" placeholder="Custom Your Word! (optional)"><?php echo $_GET['cust_word']; ?></textarea>
		<?php
		if($_GET['spiral']==1){
			echo "<select name='spiral'><option value='1'>Spiral</option><option value='0'>Spine</option></select>";
		}
		else{
			echo "<select name='spiral'><option value='0'>Spine</option><option value='1'>Spiral</option></select>";
		}
		
		if($_GET['process']==0){
			echo "<select name='process'><option value='0'>BOOKED</option><option value='1'>PAID</option><option value='2'>COMPLETED</option></select>";
		}
		else if($_GET['process']==1){
			echo "<select name='process'><option value='1'>PAID</option><option value='0'>BOOKED</option><option value='2'>COMPLETED</option></select>";
		}
		else{
			echo "<select name='process'><option value='0'>COMPLETED</option><option value='0'>BOOKED</option><option value='2'>COMPLETED</option></select>";
		}
		?>
		<br/>
		<input type="submit" name="btnEdit" value="EDIT"> <button type="reset">RESET</button> <button type="button" onClick="goBack()">BACK</button>
		<script>
		function goBack(){
			window.history.back();
		}
		</script>
	</form>
	
	<?php
	
		if(isset($_POST['btnEdit'])){
			$nama = $_POST['nama'];
			$alamat = $_POST['alamat'];
			$no_hp = $_POST['no_hp'];
			$line = $_POST['line'];
			$qty = $_POST['qty'];
			$cust_word = $_POST['cust_word'];
			$spiral = $_POST['spiral'];
			if($spiral==0){
				$harga_awal = 60000;
				$harga=$qty*$harga_awal;
			}
			else{
				$harga_awal = 60000;
				$harga = $qty*($harga_awal+8000);
			}
			$id_buku = $_POST['id_buku'];
			$process = $_POST['process'];
			$id_trans = $_GET['id_trans'];
			
			$query = mysqli_query($connect, "
			UPDATE tb_cust
			SET nama='$nama', alamat='$alamat', no_hp='$no_hp', line='$line', qty='$qty', cust_word='$cust_word', spiral='$spiral', harga='$harga', id_buku='$id_buku', process='$process'
			WHERE id_trans='$id_trans'
			") or die(mysqli_error());
			
			echo "<script>window.location.href='?module=admin&route=index'</script>";
		}
	
	?>

</body>
</html>