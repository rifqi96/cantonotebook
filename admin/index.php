<?php
if(!isset($_SESSION['username'])){
	echo "<script>window.location.href='../index.php?module=admin&route=index'</script>";
}
else{
?>
<html>
<head>
	<title>ADMIN CONTROL PANEL</title>
</head>
<body>
        <?php
		if(isset($_GET['module'])){
			include"menu.php";
		}
		?>
        
	<h2>Purchase Data</h2>
	<table border="1">
		<thead>
			<th>No.</th>
			<th>Date (YYYY-MM-DD h:m:s)</th>
			<th>Transaction ID</th>
			<th>Book ID</th>
			<th>Customer Name</th>
			<th>Address</th>
			<th>Phone No</th>
			<th>LINE ID</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Custom Word</th>
			<th>Spine/Spiral</th>
			<th>Status</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php
				$a=1;
				$query = mysqli_query($connect,"
				SELECT *
				FROM tb_cust ORDER BY id_trans ASC
				");
								
				while($row = mysqli_fetch_array($query))
				{
					$harga = number_format($row['harga']);
					if($row['process']==0){
						$status = "<a href='?module=admin&route=process&id=$row[id_trans]'><b>BOOKED</b> (Click if payment has made)</a>";
					}
					else if($row['process']==1){
						$status = "<a href='?module=admin&route=process&id=$row[id_trans]'><b>PROCESSED</b> (Click to finish)</a>";
					}
					else{
						$status = "<b>TRANSACTION COMPLETED</b>";
					}
					
					if($row['spiral']==1){
						$spiral = "Spiral";
					}
					else{
						$spiral = "Spine";
					}
				?>
				<tr>
					<td><?php echo $a; ?></td>
					<td><?php echo $row['date']; ?></td>
					<td><?php echo $row['id_trans']; ?></td>
					<td><?php echo $row['id_buku']; ?></td>
					<td><?php echo $row['nama']; ?></td>
					<td><?php echo $row['alamat']; ?></td>
					<td><?php echo $row['no_hp']; ?></td>
					<td><?php echo $row['line']; ?></td>
					<td><?php echo $row['qty']; ?></td>
					<td><?php echo $harga; ?></td>
					<td><textarea cols="50" rows="4" readonly="readonly"><?php echo $row['cust_word']; ?></textarea></td>
					<td><?php echo $spiral; ?></td>
					<td><?php echo $status; ?></td>
					<td><a href="?module=admin&route=edit&id_trans=<?php echo $row['id_trans']; ?>&id_buku=<?php echo $row['id_buku']; ?>&nama=<?php echo $row['nama']; ?>&alamat=<?php echo $row['alamat']; ?>&no_hp=<?php echo $row['no_hp']; ?>&line=<?php echo $row['line']; ?>&qty=<?php echo $row['qty']; ?>&harga=<?php echo $row['harga']; ?>&cust_word=<?php echo $row['cust_word']; ?>&spiral=<?php echo $row['spiral']; ?>&process=<?php echo $row['process']; ?>">Edit</a> | <a href="?module=admin&route=delete&id=<?php echo $row['id_trans']; ?>">Delete</a></td>
				</tr>
				<?php
					$a++;
				}
				?>
		</tbody>
	</table>

	<br>

</body>
</html>
<?php
}
?>